# Cat vs Dog image classification

Image classification on the Cat vs Dog dataset from Kaggle

**Dataset**

The full dataset contains 25,000 images of dogs and cats (1 = dog, 0 = cat).

- The images are all of different sizes
- https://www.kaggle.com/c/dogs-vs-cats/data
- ```kaggle competitions download -c dogs-vs-cats```

**Performance graphs**

1. Performance graph for training a model with 3 CNN layers (without data augmentation) on the mini cats and dogs dataset.  ```

	* Training set: 1000 images each of cats and dogs
	* validation set: 500 images each of cats and dogs
	* This model is overfitting the data, which can be addressed by data augmentation, regularization etc.
	* ![Accuracy](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/acc_cnn_without_dataAug_mini_data.jpg)
	  ![Loss](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/loss_cnn_without_dataAug_mini_data.jpg)
  
2. Performance graph for training a model with 3 CNN layers (with data augmentation) on the mini cats and dogs dataset.

	- Training set: 1000 images each of cats and dogs
	- validation set: 500 images each of cats and dogs
	- As seen from the below graph the issue of overfitting is addressed by data augmentation to some extent.
	- ![Accuracy](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/acc_cnn_dataAug_mini_data.jpg)
	  ![loss](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/loss_cnn_dataAug_mini_data.jpg)

3. Performance graph for training a model with 3 CNN layers (without data augmentation) on the full cats and dogs dataset.

	- Training set: 10000 images each of cats and dogs
	- validation set: 1250 images each of cats and dogs
	- Test set: 1250 images each of cats and dogs
	- This model is overfitting the data, which can be addressed by data augmentation, regularization etc.
	- ![Accuracy](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/acc_cnn_without_dataAug_full_data.png)
	  ![loss](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/loss_cnn_without_dataAug_full_data.png)

4. Performance graph for training a model with 3 CNN layers (with data augmentation) on the full cats and dogs dataset.

	- Training set: 10000 images each of cats and dogs
	- validation set: 1250 images each of cats and dogs
	- Test set: 1250 images each of cats and dogs
	- As seen from the below graph the issue of overfitting is addressed by data augmentation to some extent
	- ![Accuracy](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/acc_cnn_dataAug_full_data.png)
	  ![loss](https://bitbucket.org/agoel41/cat-vs-dog-image-classification/raw/93568bc6ecd488c9c4c9d0d2db1adda82e56d4ac/static/loss_cnn_dataAug_full_data.png)
