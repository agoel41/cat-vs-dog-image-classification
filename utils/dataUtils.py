import os
import random
from shutil import copyfile

__all__ = ['splitDataByCategory']

def split_data(SOURCE, TRAINING, TESTING, SPLIT_SIZE):
    files = []
    for filename in os.listdir(SOURCE):
        file = SOURCE + filename
        if os.path.getsize(file) > 0:
            files.append(filename)
        else:
            print(filename + " is zero length, so ignoring.")

    training_length = int(len(files) * SPLIT_SIZE)
    testing_length = int(len(files) - training_length)
    shuffled_set = random.sample(files, len(files))
    training_set = shuffled_set[0:training_length]
    testing_set = shuffled_set[:testing_length]

    for filename in training_set:
        this_file = SOURCE + filename
        destination = TRAINING + filename
        copyfile(this_file, destination)

    for filename in testing_set:
        this_file = SOURCE + filename
        destination = TESTING + filename
        copyfile(this_file, destination)


# split data
def splitDataByCategory(SOURCE_DIR, TRAIN_DIR, VALID_DIR, TEST_DIR, CATEGNAME, SPLITSIZE):

    files = []
    for filename in os.listdir(SOURCE_DIR):
        if (str(filename).startswith(CATEGNAME)):
            file = os.path.join(SOURCE_DIR, filename)
            if os.path.getsize(file) > 0:
                files.append(filename)
            else:
                print(filename + " is zero length, so ignoring.")

    training_length = int(len(files) * (1 - (2 * SPLITSIZE)))
    validation_length = int(len(files) * SPLITSIZE)
    testing_length = validation_length

    shuffled_set = random.sample(files, len(files))
    training_set = shuffled_set[0:training_length]
    validation_set = shuffled_set[training_length: training_length + validation_length]
    test_set = shuffled_set[training_length + validation_length: training_length + validation_length + testing_length]

    if (len(os.listdir(TRAIN_DIR)) < training_length):
        for fname in training_set:
            src = os.path.join(SOURCE_DIR, fname)
            dest = os.path.join(TRAIN_DIR, fname)
            if (not os.path.isfile(dest)):
                copyfile(src, dest)
    
    if (len(os.listdir(VALID_DIR)) < validation_length):
        for fname in validation_set:
            src = os.path.join(SOURCE_DIR, fname)
            dest = os.path.join(VALID_DIR, fname)
            if (not os.path.isfile(dest)):
                copyfile(src, dest)

    if (len(os.listdir(TEST_DIR)) < testing_length):
        for fname in test_set:
            src = os.path.join(SOURCE_DIR, fname)
            dest = os.path.join(TEST_DIR, fname)
            if (not os.path.isfile(dest)):
                copyfile(src, dest)
   
    
    

   
